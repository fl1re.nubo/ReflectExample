using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GunController : MonoBehaviour
{
    [SerializeField] private Transform _gunObj;


    [SerializeField]private float _yAxisSpeed;
    private int _yDirection;

    [SerializeField] private float _rotationSpeed;
    private float _XRotation;
    private float _YRotation;
    private float _xDirection;

    private bool IsHoldX;
    private bool IsHoldY;


    private void Update()
    {
        if (IsHoldX)
        {
            float XaxisRotation = ButtonSensevity() * _rotationSpeed;
            _XRotation -= XaxisRotation;
            _XRotation = Mathf.Clamp(_XRotation, -90f, -5f);
            _gunObj.localRotation = Quaternion.Euler(_XRotation, 0, 0f);
        }
        if (IsHoldY)
        {
            gameObject.transform.Rotate(new Vector3(0, _yAxisSpeed * _yDirection * Time.deltaTime, 0));
        }
    }

    public float ButtonSensevity()
    {
        return 0.05f * _xDirection;
    }

    public void OnHoldRotateZ(int direction)
    {
        _yDirection = direction;
        IsHoldY = true;
    }
    public void OnReleaseRotateZ()
    {
        IsHoldY = false;
    }

    public void OnHoldX(int direction)
    {
        _xDirection = direction;
        IsHoldX = true;
    }

    public void OnReleaseX()
    {
        IsHoldX = false;
    }
}
