using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserReflection : MonoBehaviour
{
	[SerializeField]private int _reflections;
	[SerializeField]private float _maxLength;
	[SerializeField]private float _currentCost;

	[SerializeField]private List<string> _names = new List<string>();
	private LineRenderer _lr;
	private Ray ray;
	private RaycastHit hit;

	private void Awake()
	{
		_lr = GetComponent<LineRenderer>();
	}

	private void Update()
	{
		ray = new Ray(transform.position, transform.forward);

		_lr.positionCount = 1;
		_lr.SetPosition(0, transform.position);
		float remainingLength = _maxLength;

		for (int i = 0; i < _reflections; i++)
		{
			if (Physics.Raycast(ray.origin, ray.direction, out hit, remainingLength))
			{
                _lr.positionCount += 1;
                _lr.SetPosition(_lr.positionCount - 1, hit.point);
                if (hit.collider.tag == "MirrorLike")
				{
					CalculateCost(hit.collider.GetComponent<CostObject>().cost, hit.collider.name);
                    remainingLength -= Vector3.Distance(ray.origin, hit.point);
                    ray = new Ray(hit.point, Vector3.Reflect(ray.direction, hit.normal));
                }
				else
                {
					_names.Clear();
					_currentCost = 5f;
				}
			}
			else
			{
				_lr.positionCount += 1;
				_lr.SetPosition(_lr.positionCount - 1, ray.origin + ray.direction * remainingLength);
			}
		}
	}

	private void CalculateCost(float costObj, string name)
	{
			if (!_names.Contains(name))
            {
				_names.Add(name);
				_currentCost -= costObj;
				if(_currentCost <= 0)
				{
					_reflections = _names.Count;
				}
            }
    }
}
